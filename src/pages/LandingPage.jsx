import React from 'react';

const LandingPage = (props) => (
    <div
        style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "90vh",
        }}>
        <h1>
            Landing Page
        </h1>
    </div>
);

export default LandingPage;
