import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Box,
    Button,
    Chip,
    Container,
    CssBaseline,
    FormControl,
    Grid,
    Input,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import DropZone from "../components/DropZone";
import PublishIcon from '@material-ui/icons/Publish';
import {newProject} from "../store/actions/projects";
import {BounceLoader} from "react-spinners";

import * as zip from "@zip.js/zip.js/dist/zip";
import DeleteIcon from "@material-ui/icons/Delete";


const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(5),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    addProjectBtn: {
        display: 'inline-block',
        textAlign: 'left',
        margin: theme.spacing(2),
        marginBottom: theme.spacing(4)
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 400,
    },
    dropZoneDiv: {
        margin: "auto"
    },
    title: {
        width: "50%",
        margin: "0 0",
    },
    submitBtn: {
        marginTop: 15
    },
    topSubmitBtn: {
        marginLeft: "2rem",
        width: "25%"
    },
    newInstanceBtn: {
        width: "25%"
    }, deleteInstanceBtn: {
        width: "75%",
        height: "75%",
        marginTop: "3.5%",
        marginLeft: "12%"
    }

});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

function getStyles(name, dependsOn, theme) {
    return {
        fontWeight:
            dependsOn.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}


class NewProjectPage extends Component {
    state = {
        projectTitle: "",
        inputFields: [
            {
                instanceName: "",
                fileName: "",
                args: "",
                dependsOn: [],
                numInstances: 1
            },
        ],
        instancesNames: [],
        file: null,
        numInstances: 0,
        errors: {
            fileError: false,
            titleError: "",
            inputFieldsError: [
                {
                    instanceNameError: "",
                    fileNameError: "",
                    argsError: "",
                },
            ],
        },
        openSnackbar: false,
        isLoading: false,
        fileAdded: false,
        fileNames: []
    }

    handleChange = (e) => {
        console.log(e.target)
        const index = e.target.name.split("-")[1]
        const values = [...this.state.inputFields];
        values[index]["dependsOn"] = e.target.value;

        this.setState({
            ...this.state,
            inputFields: values,
        })
    }

    onChange = (e, i) => {

        const values = [...this.state.inputFields];
        values[i][e.target.name] = e.target.value;
        let instancesNames;
        // if (e.target.name === "instanceName") {
        //     console.log("PASSOU")
        instancesNames = this.state.inputFields.map(inputField => inputField.instanceName)
        // }


        this.setState({
            ...this.state,
            inputFields: values,
            instancesNames: instancesNames,
        })
    }

    onChangeTitle = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    addNewInstance(e) {
        e.preventDefault()
        this.setState({
            ...this.state,
            inputFields: [
                ...this.state.inputFields,
                {
                    instanceName: "",
                    fileName: "",
                    args: "",
                    dependsOn: [],
                    numInstances: 1
                },
            ],
        })
    }

    deleteInstance = (e, i) => {
        if (this.state.inputFields.length > 1) {
            this.state.errors.inputFieldsError.splice(i, 1)
            this.state.inputFields.splice(i, 1)
            this.setState({...this.state})
        }
    }
    validate = () => {
        let isErr = false;
        const errors = {
            titleError: "",
            inputFieldsError: [],
            fileError: "",
        };

        this.state.inputFields.forEach(() => errors.inputFieldsError.push({
            instanceNameError: "",
            fileNameError: "",
        }))

        if (this.state.projectTitle.length <= 0) {
            isErr = true;
            errors.titleError = "Title is required";
            console.log("Title is required")
        } else if (!this.state.projectTitle.length > 65) {
            isErr = true;
            errors.titleError = "Title is too big";
            console.log("Title is too big")
        }

        this.state.inputFields.forEach((inputField, i) => {
            if (inputField.instanceName.length <= 0) {
                isErr = true;
                errors.inputFieldsError[i]["instanceNameError"] = "Instance Name is required";
                console.log(`Instance Name is required at: ${i}`)
            } else if (inputField.instanceName.length > 65) {
                isErr = true;
                errors.inputFieldsError[i]["instanceNameError"] = "Instance Name is too big";
                console.log(`Instance Name is too big at: ${i}`)
            }

            if (inputField.fileName.length <= 0) {
                isErr = true;
                errors.inputFieldsError[i]["fileNameError"] = "File Name is required";
                console.log(`File Name is required at: ${i}`)
            } else if (inputField.instanceName.length > 65) {
                isErr = true;
                errors.inputFieldsError[i]["fileNameError"] = "File Name is too big";
                console.log(`File Name is too big at: ${i}`)
            }
        })

        if (this.state.file === null) {
            errors.fileError = true
        }


        this.setState({
            ...this.state,
            errors: {
                ...errors
            },
        })

        return isErr;
    }

    handleSubmit = (e) => {
        e.preventDefault()

        console.log(this.state)
        // console.log(Array.from(formData))
        const err = this.validate();
        // console.log("handleSubmit-err", err)
        if (!err) {
            //if no err create new project
            this.setState({
                ...this.state,
                isLoading: true
            })
            delete this.state.errors
            delete this.state.openSnackbar
            delete this.state.instancesNames
            this.props.createNewProject(this.state)
        }
    };

    fileAdded = async (wasAdded) => {
        this.setState({
            ...this.state,
            fileAdded: wasAdded
        })
        // create a BlobReader to read with a ZipReader the zip from a Blob object
        const reader = new zip.ZipReader(new zip.BlobReader(this.state.file));

        // get all entries from the zip
        const entries = await reader.getEntries();
        if (entries.length) {

            if (entries.length) {
                console.log("entries: ", entries)
                let jarFilesNames = []
                entries.forEach(e => {
                    if (e.filename.includes(".jar")) {
                        jarFilesNames.push(e.filename)
                    }
                })
                this.setState({
                    ...this.state,
                    fileNames: jarFilesNames
                })
                console.log("jarFilesNames: ", jarFilesNames)
            }
        }
        // close the ZipReader
        await reader.close();

    }


    render() {
        const {classes, theme} = this.props;

        console.log("WELE-this.state.inputFields: ", this.state.inputFields)
        return (
            <Container maxWidth="lg">
                <Box py={3}>
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Grid container component="main" spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="h4" align="left">
                                    New Project
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="standard"
                                    margin="normal"
                                    required
                                    id="projectTitle"
                                    label="Project Title"
                                    name="projectTitle"
                                    autoComplete="projectTitle"
                                    autoFocus
                                    disabled={this.state.isLoading}
                                    value={this.state.projectTitle}
                                    error={this.state.errors.titleError !== undefined && this.state.errors.titleError !== ""}
                                    helperText={this.state.errors.titleError}
                                    onChange={e => this.onChangeTitle(e)}
                                    className={classes.title}
                                />

                            </Grid>

                            <Grid item xs={12}>
                                <div className={classes.dropZoneDiv}>
                                    <DropZone parentState={this.state} fileAdded={this.fileAdded}
                                              disabled={this.state.isLoading} error/>
                                    {this.state.errors.fileError ?
                                        <Typography variant="h6" align="center" style={{color: "red"}}>
                                            Zip file is required
                                        </Typography>
                                        : null}
                                </div>
                            </Grid>
                            {
                                this.state.inputFields.map((inputField, index) => (

                                    <>
                                        <Grid item xs={6} sm={4}>
                                            <TextField
                                                variant="outlined"
                                                margin="normal"
                                                required
                                                id="instanceName"
                                                label="Instance Name"
                                                name="instanceName"
                                                autoComplete="instanceName"
                                                fullWidth
                                                disabled={this.state.isLoading}
                                                value={inputField.instanceName}
                                                error={this.state.errors.inputFieldsError[index] !== undefined &&
                                                this.state.errors.inputFieldsError[index].instanceNameError !== ""}
                                                helperText={this.state.errors.inputFieldsError[index] !== undefined ? this.state.errors.inputFieldsError[index].instanceNameError : ""}
                                                onChange={e => this.onChange(e, index)}

                                            />
                                        </Grid>
                                        <Grid item xs={6} sm={4}>
                                            <TextField
                                                id="fileName"
                                                select
                                                label="File Name"
                                                value={inputField.fileName}
                                                name={"fileName"}
                                                onChange={e => this.onChange(e, index)}
                                                variant="outlined"
                                                fullWidth
                                                required
                                                margin={"normal"}
                                                disabled={this.state.fileNames.length === 0 || this.state.isLoading}
                                                error={this.state.errors.inputFieldsError[index] !== undefined &&
                                                this.state.errors.inputFieldsError[index].fileNameError !== ""}
                                                helperText={this.state.errors.inputFieldsError[index] !== undefined ? this.state.errors.inputFieldsError[index].fileNameError : ""}
                                            >
                                                {this.state.fileNames.map((option) => (
                                                    <MenuItem key={option} value={option}>
                                                        {option}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </Grid>
                                        <Grid item xs={6} sm={4}>
                                            <TextField
                                                variant="outlined"
                                                margin="normal"
                                                fullWidth
                                                id="args"
                                                label="Arguments"
                                                name="args"
                                                autoComplete="args"
                                                multiline={true}
                                                rowsMax={4}
                                                disabled={this.state.isLoading}
                                                value={inputField.args}
                                                onChange={e => this.onChange(e, index)}
                                                placeholder={"arg[0],arg[1]... (Eg.)"}
                                            />
                                        </Grid>
                                        <Grid item xs={6} sm={4}>
                                            <FormControl className={classes.formControl} fullWidth={true}
                                                         margin={"normal"}>
                                                <InputLabel id="dependsOnInputLabel">Depends On:</InputLabel>
                                                <Select
                                                    labelId="dependsOnInputLabel"
                                                    id="dependsOnInputChip"
                                                    multiple
                                                    value={inputField.dependsOn}
                                                    name={`dependsOnInputChip-${index}`}
                                                    onChange={this.handleChange}
                                                    input={<Input id="dependsOnInputChip"/>}
                                                    renderValue={(selected) => (
                                                        <div className={classes.chips}>
                                                            {selected.map((value) => (
                                                                <Chip key={value} label={value}
                                                                      className={classes.chip}/>
                                                            ))}
                                                        </div>
                                                    )}
                                                    fullWidth={true}
                                                    disabled={this.state.inputFields.length <= 1 || this.state.isLoading}
                                                    MenuProps={MenuProps}
                                                >
                                                    {this.state.instancesNames.map((name) => (
                                                        <MenuItem key={name} value={name}
                                                                  style={getStyles(name, inputField.dependsOn, theme)}>
                                                            {name}
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6} sm={4}>

                                            <TextField
                                                variant="outlined"
                                                margin="normal"
                                                required
                                                fullWidth
                                                id="numInstances"
                                                label="Number of instances:"
                                                name="numInstances"
                                                autoComplete="numInstances"
                                                type="number"
                                                placeholder={1}
                                                disabled={this.state.isLoading}
                                                defaultValue={1}
                                                inputProps={{min: 1}}
                                                onChange={e => this.onChange(e, index)}
                                            />
                                        </Grid>
                                        <Grid item xs={6} sm={4}>
                                            <Button
                                                startIcon={<DeleteIcon/>}
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className={classes.deleteInstanceBtn}
                                                disabled={this.state.inputFields.length <= 1}
                                                onClick={e => this.deleteInstance(e, index)}
                                            >
                                                Delete this instance
                                            </Button>
                                        </Grid>
                                    </>
                                ))
                            }
                            <Grid item xs={12}>
                                <Button
                                    startIcon={<AddIcon/>}
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    disabled={this.state.isLoading}
                                    // className={classes.submit}
                                    className={classes.newInstanceBtn}
                                    onClick={e => this.addNewInstance(e)}
                                >
                                    Add New Instance
                                </Button>
                                <Button
                                    startIcon={(!this.state.isLoading ? <PublishIcon/> : <BounceLoader size={20}/>)}
                                    type="button"
                                    variant="contained"
                                    color="secondary"
                                    disabled={this.state.isLoading}
                                    // className={classes.submit}
                                    className={classes.topSubmitBtn}
                                    onClick={e => this.handleSubmit(e)}
                                >
                                    Submit
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Box>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {createNewProject: (projectData) => dispatch(newProject(projectData))};
}

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles, {withTheme: true})(NewProjectPage)));
