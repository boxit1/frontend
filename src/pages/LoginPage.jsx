import React, {Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {Snackbar, withStyles} from "@material-ui/core";
import Copyright from "../components/Copyright";
import {connect} from "react-redux";
import {clearErrMsg, login} from "../store/actions/auth";
import Alert from '@material-ui/lab/Alert';

const styles = (theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/featured/?Programming)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
});

class LoginPage extends Component {
    state = {
        email: "",
        password: "",
        errors: {
            emailError: "",
            passwordError: ""
        },
        openSnackbar: false
    }

    validate = () => {
        let isErr = false;
        const errors = {
            emailError: "",
            passwordError: "",
        };
        const emailRegex = new RegExp("^[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$")
        const passRegex = new RegExp("^.[^()<>[\\]{}=]+$")

        if (this.state.email.length <= 0) {
            isErr = true;
            errors.emailError = "Email is required";
            console.log("Email is required")
        } else if (!emailRegex.test(this.state.email)) {
            isErr = true;
            errors.emailError = "Invalid Email";
            console.log("Invalid Email")
        }
        if (this.state.password.length <= 0) {
            isErr = true;
            errors.passwordError = "Password is required";
            console.log("Password is required")
        } else if (!passRegex.test(this.state.password)) {
            isErr = true;
            errors.passwordError = "Invalid Password";
            console.log("Invalid Password")
        }

        this.setState({
            ...this.state,
            errors: {
                ...errors
            },
        })

        return isErr;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();

        const err = this.validate();
        // console.log("handleSubmit-err", err)
        if (!err) {
            //if no err do Login
            delete this.state.errors
            delete this.state.openSnackbar
            this.props.doLogin(this.state)
        }
    };


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.isAuthenticated) {
            this.props.history.push('/home')
        }

        if (prevProps !== this.props) {
            this.setState({
                ...this.state,
                openSnackbar: !!this.props.serverErrMsg,
            })
        }
    };

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            ...this.state,
            openSnackbar: false
        });
        this.props.clearErrMsg();
    };

    onKeyDown = (event: React.KeyboardEvent<HTMLDivElement>): void => {
        // 'keypress' event misbehaves on mobile so we track 'Enter' key via 'keydown' event
        if (event.key === 'Enter' || event.key === "NumpadEnter") {
            // event.preventDefault();
            // event.stopPropagation();
            this.handleSubmit(event);
        }
    }

    render() {
        const {classes} = this.props;

        return (
            <Grid container component="main" className={classes.root}>
                <CssBaseline/>
                <Snackbar open={this.state.openSnackbar} autoHideDuration={6000} onClose={this.handleClose}>
                    <Alert variant="filled" onClose={this.handleClose} severity="error">
                        {this.props.serverErrMsg}
                    </Alert>
                </Snackbar>
                <Grid item xs={false} sm={4} md={7} className={classes.image}/>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Login
                        </Typography>
                        <form
                            className={classes.form}
                            // noValidate
                            // onSubmit={this.props.doLogin}>
                            // onSubmit={this.props.doLogin}>
                        >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                value={this.state.email}
                                error={this.state.errors.emailError !== ""}
                                helperText={this.state.errors.emailError}
                                onChange={e => this.onChange(e)}
                                onKeyDown={this.onKeyDown}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                error={this.state.errors.passwordError !== ""}
                                helperText={this.state.errors.passwordError}
                                value={this.state.password}
                                onChange={e => this.onChange(e)}
                                onKeyDown={this.onKeyDown}
                            />
                            {/*<FormControlLabel*/}
                            {/*    control={<Checkbox value="remember" color="primary"/>}*/}
                            {/*    label="Remember me"*/}
                            {/*/>*/}
                        </form>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={e => this.handleSubmit(e)}
                        >
                            Login
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2">
                                    Forgot password?
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href="#" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                        <Box mt={5}>
                            <Copyright/>
                        </Box>
                    </div>
                </Grid>
            </Grid>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    doLogin: (userCreds) => dispatch(login(userCreds)),
    clearErrMsg: () => dispatch(clearErrMsg()),
});

function mapStateToProps(state) {
    // console.log("mapStateToProps-state.auth.errMsg", state.auth.errMsg)
    return {
        isAuthenticated: state.auth.isLoggedin,
        serverErrMsg: state.auth.errMsg,
    };
}


export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(LoginPage)))
