import {
    CLEAR_ERR_MSG,
    GET_BACKEND_MODE,
    LOGIN,
    LOGIN_ERR,
    LOGIN_SINGLE_USER,
    REFRESH_TOKEN,
    USER_LOGOUT
} from "../actions/actionTypes";

const defaultState = {
    isLoggedin: false
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case LOGIN_SINGLE_USER: {
            return {
                ...state,
                userName: "Single User",
                isLoggedin: true
            };
        }
        case LOGIN: {
            const {userName, token, refreshToken} = action.payload.data
            return {
                ...state,
                userName: userName,
                tokenJWT: token,
                refreshTokenJWT: refreshToken,
                isLoggedin: true
            };
        }
        case LOGIN_ERR:
            // console.log("LOGIN_ERR-action.payload: ", action.payload)
            return {
                ...state,
                errMsg: action.payload
            };
        case CLEAR_ERR_MSG:
            // console.log("CLEAR_ERR_MSG RUNNED")
            return {
                ...state,
                errMsg: ""
            };
        case USER_LOGOUT:
            return {
                isLoggedin: false
            };
        case REFRESH_TOKEN:
            return {
                ...state,
                tokenJWT: action.payload
            };
        case GET_BACKEND_MODE:
            return {
                ...state,
                backendSingleUser: action.payload
            };
        default:
            return state;
    }
};
