import {ADD_PROJECT, DELETE_PROJECT, GET_PROJECTS, UPDATE_PROJECT} from "../actions/actionTypes";

const defaultState = {
    projects: [],
}

export default (state = defaultState, action) => {

    console.log("projectsReducer-action: ", action)
    console.log("STATE: ", state)
    switch (action.type) {
        case GET_PROJECTS: {
            return {
                projects: action.payload
            };
        }
        case ADD_PROJECT: {
            return {
                projects: [...state.projects, action.payload]
            };
        }
        case DELETE_PROJECT: {
            return {
                // projects: [...state.projects, action.payload]
            };
        }
        case UPDATE_PROJECT: {
            let projects = state.projects
            console.log("action.payload.project: ", action.payload.project)
            console.log("projects1: ", projects)
            let index = projects.indexOf(
                projects.find(value => value.projectUUID === action.payload.projectUuid)
            )

            console.log("index: ", index)
            projects[index] = action.payload.project
            console.log("projects2: ", projects)
            return {
                projects: projects
            };
        }
        default:
            return state;
    }
};
