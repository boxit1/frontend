import axios from "axios";

export default function setAuthToken(token) {
    // console.log("setAuthToken Runned - token: ", token)
    if (token) {
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    } else {
        delete axios.defaults.headers.common["Authorization"];
    }
}
