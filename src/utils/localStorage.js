export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state')
        if (serializedState === null) {
            //caso nao exista estado guardado a store cria o seu por defeito
            return undefined
        }
        return JSON.parse(serializedState)
    } catch (err) {
        //caso nao exista estado guardado a store cria o seu por defeito ou as predefiniçoes do cliente nao de para local storage
        return undefined
    }
}

export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('state', serializedState)
    } catch (err) {
        //Loggar os erros
    }
}