import React from 'react';
import ReactDom from "react-dom";
import {makeStyles, MuiThemeProvider} from "@material-ui/core/styles";
import {Button, createMuiTheme, Typography} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import CancelIcon from '@material-ui/icons/Cancel';
import {blue, red} from '@material-ui/core/colors'

const useStyles = makeStyles({
    modalStyles: {
        position: 'fixed',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: '#FFF',
        padding: '50px',
        borderRadius: '5px',
        zIndex: 1000
    },
    overLayStyles: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, .7)',
        zIndex: 1000
    }, buttons: {
        display: "flex",
        justifyContent: "space-between",
        margin: "3rem 0 0 0"
    }, text: {}
});

function Modal({open, children, onClose, onDeleteClick}) {
    const classes = useStyles();

    const redTheme = createMuiTheme({palette: {primary: red}})
    const blueTheme = createMuiTheme({palette: {primary: blue}})

    if (!open) return null

    function onDeleteClickAndClose() {
        onDeleteClick()
        onClose()
        window.location.reload(false);
        return window.location.href = "/projects"
    }

    return ReactDom.createPortal(
        <>
            <div className={classes.overLayStyles}/>
            <div className={classes.modalStyles}>
                <Typography variant="h5">{children}</Typography>
                <div className={classes.buttons}>
                    <MuiThemeProvider theme={redTheme}>
                        <Button
                            color="primary"
                            variant="contained"
                            startIcon={<DeleteIcon/>}
                            size="medium"
                            onClick={onDeleteClickAndClose}
                        >
                            Delete
                        </Button>
                    </MuiThemeProvider>
                    <MuiThemeProvider theme={blueTheme}>
                        <Button
                            color="primary"
                            variant="contained"
                            size="medium"
                            startIcon={<CancelIcon/>}
                            onClick={onClose}
                        >
                            Cancel
                        </Button>
                    </MuiThemeProvider>
                </div>
            </div>
        </>,
        document.getElementById('portal')
    )
}

export default Modal;