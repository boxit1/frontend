import React, {Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {Snackbar, withStyles} from "@material-ui/core";
import Copyright from "../components/Copyright";
import Link from "@material-ui/core/Link";
import {Link as RouterLink} from 'react-router-dom';
import {connect} from "react-redux";
import {clearErrMsg, signup} from "../store/actions/signup";
import SignUpSuccess from "./SignUpSuccess";
import Alert from "@material-ui/lab/Alert";

const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        marginTop: '1rem',
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
});

class SignUpPage extends Component {
    state = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        errors: {
            firstNameError: "",
            lastNameError: "",
            emailError: "",
            passwordError: "",
            confirmPasswordError: "",
        },
        openSnackbar: false,
        success: false,
    }

    validate = () => {
        let isErr = false;
        const errors = {
            firstNameError: "",
            lastNameError: "",
            emailError: "",
            passwordError: "",
            confirmPasswordError: "",
        };

        const nameRegex = new RegExp("^[^0-9.,\"?!;:#$%&()*+-/<>=@[\\]\\^\\_\\{\\}\\|\\~]+$")
        const emailRegex = new RegExp("^[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$")
        const passRegex = new RegExp("^.[^()<>[\\]{}=]+$")
        if (this.state.firstName.length <= 0) {
            isErr = true;
            errors.firstNameError = "First Name is required";
            console.log("First Name is required")
        } else if (this.state.firstName.length <= 2) {
            isErr = true;
            errors.firstNameError = "Your input is too short";
            console.log("Your input is too short")
        } else if (this.state.firstName.length > 12) {
            isErr = true;
            errors.firstNameError = "Your input is too short";
            console.log("Your input is too short")
        } else if (!nameRegex.test(this.state.firstName)) {
            isErr = true;
            errors.firstNameError = "Invalid name";
            console.log("Invalid name")
        }

        if (this.state.lastName.length <= 0) {
            isErr = true;
            errors.lastNameError = "Last Name is required";
            console.log("Last Name is required")
        } else if (this.state.lastName.length <= 2) {
            isErr = true;
            errors.lastNameError = "Your input is too short";
            console.log("Your input is too short")
        } else if (this.state.lastName.length > 12) {
            isErr = true;
            errors.lastNameError = "Your input is too short";
            console.log("Your input is too short")
        } else if (!nameRegex.test(this.state.lastName)) {
            isErr = true;
            errors.lastNameError = "Invalid name";
            console.log("Invalid name")
        }


        if (this.state.email.length <= 0) {
            isErr = true;
            errors.emailError = "Email is required";
            console.log("Email is required")
        } else if (!emailRegex.test(this.state.email)) {
            isErr = true;
            errors.emailError = "Invalid Email";
            console.log("Invalid Email")
        }

        if (this.state.password.length <= 0) {
            isErr = true;
            errors.passwordError = "Password is required";
            console.log("Password is required")
        } else if (this.state.password.length <= 5) {
            isErr = true;
            errors.passwordError = "Your input is too short";
            console.log("Your input is too short")
        } else if (!passRegex.test(this.state.password)) {
            isErr = true;
            errors.passwordError = "Invalid Password";
            console.log("Invalid Password")
        } else if (this.state.password !== this.state.confirmPassword) {
            isErr = true;
            errors.passwordError = "Password does not match";
            console.log("Password does not match")
        }

        if (this.state.confirmPassword.length <= 0) {
            isErr = true;
            errors.confirmPasswordError = "Confirm Password is required";
            console.log("Confirm Password is required")
        } else if (this.state.confirmPassword.length <= 5) {
            isErr = true;
            errors.confirmPasswordError = "Your input is too short";
            console.log("Your input is too short")
        } else if (!passRegex.test(this.state.confirmPassword)) {
            isErr = true;
            errors.confirmPasswordError = "Invalid confirmPassword";
            console.log("Invalid confirmPassword")
        } else if (this.state.password !== this.state.confirmPassword) {
            isErr = true;
            errors.confirmPasswordError = "Password does not match";
            console.log("Password does not match")
        }

        this.setState({
            ...this.state,
            errors: {
                ...errors
            },
        })

        return isErr;
    }

    handleSubmit = (e) => {
        e.preventDefault()

        // console.log("handleSubmit-state: ", this.state)

        const err = this.validate();
        // console.log("handleSubmit-err", err)
        if (!err) {
            //if no err do Login
            delete this.state.errors
            delete this.state.openSnackbar
            // console.log("handleSubmit-state (inside if): ", this.state)

            this.props.doSignUp(this.state)
        }
    };


    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log("componentDidUpdate-this.props.signUpMsg: ", this.props.signUpMsg)
        if (prevProps !== this.props) {
            this.setState({
                ...this.state,
                openSnackbar: !!this.props.signUpMsg,
            })
        }
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({
            ...this.state,
            openSnackbar: false
        });
        this.props.clearErrMsg();
    };

    render() {
        const {classes} = this.props;

        if (this.props.signUpMsgType === 'success') {

            return <SignUpSuccess {...this.props}/>
        } else {
            return (
                <Container component="main" maxWidth="xs">
                    <CssBaseline/>
                    <Snackbar open={this.state.openSnackbar} autoHideDuration={6000} onClose={this.handleClose}>
                        <Alert variant="filled" onClose={this.handleClose} severity="error">
                            {this.props.signUpMsg}
                        </Alert>
                    </Snackbar>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign up
                        </Typography>
                        <form className={classes.form} noValidate>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        autoComplete="fname"
                                        name="firstName"
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="firstName"
                                        label="First Name"
                                        autoFocus
                                        onChange={e => this.onChange(e)}
                                        value={this.state.firstName}
                                        error={this.state.errors.firstNameError !== ""}
                                        helperText={this.state.errors.firstNameError}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="lname"
                                        onChange={e => this.onChange(e)}
                                        value={this.state.lastName}
                                        error={this.state.errors.lastNameError !== ""}
                                        helperText={this.state.errors.lastNameError}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                        onChange={e => this.onChange(e)}
                                        value={this.state.email}
                                        error={this.state.errors.emailError !== ""}
                                        helperText={this.state.errors.emailError}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                        onChange={e => this.onChange(e)}
                                        value={this.state.password}
                                        error={this.state.errors.passwordError !== ""}
                                        helperText={this.state.errors.passwordError}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="confirmPassword"
                                        label="Confirm Password"
                                        type="password"
                                        id="confirmPassword"
                                        autoComplete="current-password"
                                        onChange={e => this.onChange(e)}
                                        value={this.state.confirmPassword}
                                        error={this.state.errors.confirmPasswordError !== ""}
                                        helperText={this.state.errors.confirmPasswordError}
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={e => this.handleSubmit(e)}
                            >
                                Sign Up
                            </Button>
                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link variant="body2" component={RouterLink} to="/login">
                                        Already have an account? Sign in
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                    <Box mt={5}>
                        <Copyright/>
                    </Box>
                </Container>
            );

        }
    }
}

const mapDispatchToProps = (dispatch) => ({
    doSignUp: (userData) => dispatch(signup(userData)),
    clearErrMsg: () => dispatch(clearErrMsg()),
});

function mapStateToProps(state) {
    // console.log("mapStateToProps-state.auth.errMsg", state.auth.errMsg)
    return {
        signUpMsgType: state.signUp.msgType,
        signUpMsg: state.signUp.signUpMsg
        // isAuthenticated: state.auth.isLoggedin,
        // serverErrMsg: state.auth.errMsg,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(SignUpPage)));
