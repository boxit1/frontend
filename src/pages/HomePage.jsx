import React from 'react';
import {useSelector} from "react-redux";

const HomePage = (props) => {
    const isLogged = useSelector((state) => state.auth.isLoggedin);
    const name = useSelector((state) => state.auth.userName);

    console.log(isLogged, name)
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "90vh",
            }}
        >
            {!isLogged ? (
                <h1>Home</h1>

            ) : (
                <h1>Welcome {name}</h1>
            )
            }

        </div>
    );
};

export default HomePage;
