import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {Avatar} from "@material-ui/core";
import {Link} from "react-router-dom";
import Modal from "./Modal";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    }, flexContainer: {
        display: 'flex',
        flexDirection: 'row',
    }, avatar: {
        alignItems: "center",
        alignSelf: "center",
    }, title: {
        margin: 'auto',
        marginLeft: '1rem',
        fontWeight: 'bold',
    }, description: {
        fontSize: "large",
        marginTop: '1rem',
    }, link: {
        textDecoration: "none",
        color: "inherit"
    }, avatarRunning: {
        backgroundColor: "rgba(82,255,85,0.82)"
    }, avatarOther: {
        backgroundColor: "rgba(255,162,0,0.82)"
    }, avatarStoped: {
        backgroundColor: "rgba(255,34,34,0.82)"
    }
});

export default function ProjectCard({
                                        title = "Project",
                                        state = "N/A",
                                        runningInst = "N/A",
                                        propsToLink = null,
                                        onDelete = null
                                    }) {

    const [isOpen, setIsOpen] = useState(false)

    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <Link to={propsToLink} className={classes.link}>
                    <CardContent>
                        <div className={classes.flexContainer}>
                            <Avatar
                                className={[classes.avatar,
                                    state === 'running' ? classes.avatarRunning :
                                        state === 'stoped' ? classes.avatarStoped : classes.avatarOther]}
                            >
                                {title ? (title.substr(0, 2)) : ("P1")}

                                {/*P1*/}
                            </Avatar>

                            <Typography
                                gutterBottom
                                variant="h5"
                                component="h2"
                                className={classes.title}
                            >
                                {title}
                            </Typography>

                        </div>
                        <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                            className={[classes.description]}
                        >
                            State: {state}<br/>
                            Running instances: {runningInst}
                        </Typography>
                    </CardContent>
                </Link>
            </CardActionArea>
            <CardActions>
                <Link to={propsToLink} className={classes.link}>
                    <Button size="small" color="primary">
                        Open
                    </Button>
                </Link>
                <Button size="small" color="primary" onClick={() => setIsOpen(true)}>
                    Delete
                </Button>
            </CardActions>
            <Modal open={isOpen} onClose={() => setIsOpen(false)} onDeleteClick={onDelete}>
                Are you sure you want to delete this project?
            </Modal>
        </Card>
    );
}
