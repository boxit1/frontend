import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';

export const PublicRoute = ({
                                isAuthenticated,
                                component: Component,
                                ...rest
                            }) => (

    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <Redirect to="/home"/>
        ) : (
            <React.Fragment>
                <Component {...props} />
            </React.Fragment>
        )
    )}/>
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.isLoggedin
});

export default connect(mapStateToProps)(PublicRoute);
