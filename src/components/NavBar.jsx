import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {AppBar, Button} from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {Link} from "react-router-dom";
import {FaBoxOpen} from "react-icons/fa";
import TransitEnterexitOutlinedIcon from '@material-ui/icons/TransitEnterexitOutlined';
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../store/actions/auth";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    flexContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
    tabs: {
        flex: 'auto'
    }, signupButton: {
        flex: "initial",

        borderRadius: '4px',
        background: '#fff',
        margin: '.25rem',
        height: '2.5rem',
        width: '5.5rem',
        color: '#1976d2',
        cursor: 'pointer',
        transition: ' all 0.2s ease-in-out',
        textDecoration: 'none',
        fontWeight: 'bold',

        borderColor: '#1976d2',
        borderWidth: '2px',
        borderStyle: "solid",

        display: 'flex',
        alignItems: 'center',

        '&:hover': {
            transition: 'all 0.2s ease-in-out',
            background: '#1976d2',
            color: '#fff',
            borderColor: '#fff',
        },

    },
    loginButton: {
        flex: "initial",

        borderRadius: '4px',
        background: '#1976d2',
        margin: '.25rem',
        height: '2.5rem',
        width: '5.5rem',
        marginRight: '5rem',
        color: '#fff',
        cursor: 'pointer',
        transition: ' all 0.2s ease-in-out',
        textDecoration: 'none',
        fontWeight: 'bold',

        display: 'flex',
        alignItems: 'center',

        '&:hover': {
            transition: 'all 0.2s ease-in-out',
            background: '#fff',
            color: '#1976d2',
        },
        [theme.breakpoints.down('sm')]: {
            marginRight: '.2rem',
        },

    },
    link: {
        textDecoration: "none",
    }, logo: {
        margin: '0px',
        color: '#fff',
        fontSize: '2rem',
    }, logoText: {
        margin: '0px',
        marginLeft: ".5rem",
        color: '#1976d2',
        fontSize: '1.rem'
    }, logoLink: {
        marginLeft: '5rem',
        marginRight: '1rem',
        textDecoration: "none",
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        [theme.breakpoints.down('sm')]: {
            marginLeft: '.2rem',
        }
    }, rotate180: {
        transform: "rotate(180deg)"
    }, logoutButton: {
        flex: "initial",

        borderRadius: '4px',
        background: '#f44336',
        margin: '.25rem',
        height: '2.5rem',
        width: '5.5rem',
        marginRight: '5rem',
        color: '#fff',
        cursor: 'pointer',
        transition: ' all 0.2s ease-in-out',
        textDecoration: 'none',
        fontWeight: 'bold',

        display: 'flex',
        alignItems: 'center',

        '&:hover': {
            transition: 'all 0.2s ease-in-out',
            background: '#fff',
            color: '#f44336',
        },
        [theme.breakpoints.down('sm')]: {
            marginRight: '.2rem',
        },

    },
}));

const NavBar = ({history}) => {
    const classes = useStyles();
    const isLogged = useSelector((state) => state.auth.isLoggedin);
    const isSingleUserMode = useSelector((state) => state.auth.backendSingleUser);
    const dispatch = useDispatch()

    function onLogoutClick() {
        dispatch(logout())
    }

    return (

        <AppBar className={classes.root}>
            <div className={classes.flexContainer}>
                <Link className={classes.logoLink} to='/'>
                    <FaBoxOpen className={classes.logo}/>
                    <h1 className={classes.logoText}>It</h1>
                </Link>
                <Tabs
                    value={history.location.pathname}
                    centered
                    className={classes.tabs}
                >
                    <Tab
                        label='Landing'
                        value="/"
                        component={Link}
                        to="/"/>
                    <Tab
                        label='Home'
                        value="/home"
                        component={Link}
                        to="/home"/>
                    {isLogged ?
                        <Tab
                            label='Projects'
                            value="/projects"
                            component={Link}
                            to="/projects"/> : null
                    }
                </Tabs>

                {!isSingleUserMode ? !isLogged ? (
                    <>
                        <Link className={classes.link} to={'/signup'}>
                            <Button
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                color="inherit"
                                className={classes.signupButton}
                            >
                                <TransitEnterexitOutlinedIcon/>
                                SignUp
                            </Button>
                        </Link>
                        <Link className={classes.link} to={'/login'}>
                            <Button
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                color="inherit"
                                className={classes.loginButton}
                            >
                                <ExitToAppIcon/>
                                Login
                            </Button>
                        </Link>
                    </>

                ) : (
                    <>
                        <Link className={classes.link} to={'/logout'}>
                            <Button
                                aria-label="logout"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                color="inherit"
                                className={classes.logoutButton}
                                onClick={() => onLogoutClick()}
                            >
                                <ExitToAppIcon className={classes.rotate180}/>
                                Logout
                            </Button>
                        </Link>
                    </>
                ) : null
                }
            </div>
        </AppBar>
    );
}

export default NavBar;
