import {Box, Container, CssBaseline, makeStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CheckCircleOutlineSharpIcon from "@material-ui/icons/CheckCircleOutlineSharp";
import React from "react";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(7),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        marginTop: theme.spacing(3),
        margin: theme.spacing(1),
        color: theme.palette.primary.main,
        backgroundColor: "none",
        height: "6rem",
        width: "6rem",
    },
}));

const Success = (props) => {
    const classes = useStyles();
    const changeToLogin = () => {
        props.history.push("/login");
    };
    return (
        <Container component="main" maxWidth="sm">
            <CssBaseline/>
            <div className={classes.paper}>
                <Box>
                    <CheckCircleOutlineSharpIcon
                        fontSize="large"
                        className={classes.avatar}
                    />
                </Box>
                <Box mt={3}>
                    <Typography component="h1" variant="h5">
                        Activation email sent successfully.
                    </Typography>
                    <Typography component="h1" variant="h6">
                        Please confirm and then Login.
                    </Typography>
                </Box>
                <Box my={5}>
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={changeToLogin}
                    >
                        Login
                    </Button>
                </Box>
            </div>
        </Container>
    );
};

export default Success;
