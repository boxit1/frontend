import axios from "axios";
import {CLEAR_ERR_MSG, SIGNUP_SHOW_ERR_MSG, SIGNUP_SHOW_MSG} from "./actionTypes";
// export const login = (data) => ({
//     type: 'LOGIN',
//     payload: data,
// });
export const signup = (data) => {
    // console.log("data: ", data)

    return (dispatch) => {
        axios.post('/signUp', {...data, role: 'user'})
            .then((res) => {
                // console.log("signup-axios.then-res: ", res)
                // console.log("signup-axios.then-res.data.message: ", res.data.message)
                dispatch({
                    type: SIGNUP_SHOW_MSG,
                    payload: res.data.message
                })
            })
            .catch((err) => {
                // console.log("catch-err.response.data.message: ", err.response.data.message)
                // console.log(err)
                dispatch({
                    type: SIGNUP_SHOW_ERR_MSG,
                    payload: err.response.data.message
                })
            });
    };
};

export const clearErrMsg = () => ({
    type: CLEAR_ERR_MSG
})
