import axios from "axios";
import {
    CLEAR_ERR_MSG,
    GET_BACKEND_MODE,
    LOGIN,
    LOGIN_ERR,
    LOGIN_SINGLE_USER,
    REFRESH_TOKEN,
    USER_LOGOUT
} from "./actionTypes";
import setAuthToken from "../../utils/setAuthToken";
// export const login = (data) => ({
//     type: 'LOGIN',
//     payload: data,
// });
export const login = (data) => {
    // console.log("data: ", data)

    return (dispatch) => {
        axios.post('/login', data)
            .then((res) => {
                // console.log("then-res.data: ", res.data)
                dispatch({
                    type: LOGIN,
                    payload: res.data
                })
                setAuthToken(res.data.data.token)
            })
            .catch((err) => {
                // console.log("catch-err.response: ", err.response)
                if (err.response) {
                    // console.log("catch-err.response.data.message: ", err.response.data.message)
                    // throw new Error(err.response.data.message);
                    dispatch({
                        type: LOGIN_ERR,
                        payload: err.response.data.message
                    })
                }
            });
    };
};
export const logout = () => {
    // type: USER_LOGOUT

    return (dispatch) => {
        console.log("Dispatching now logout")
        axios.post('/logout')
            .then((res) => {
                console.log("logout-then.data: ", res.data)
                dispatch({
                    type: USER_LOGOUT
                })
                window.location.href = "/"
            })
            .catch((err) => {
                console.error("logout-err.response: ", err.response)
            });
        dispatch({
            type: USER_LOGOUT
        })
    };
};


export const refreshToken = (newToken) => ({
    type: REFRESH_TOKEN,
    payload: newToken
})

// export const refreshToken = (originalReq) => {
//     // console.log("data: ", data)
//
//     return (dispatch, getState) => {
//         axios.post('/refreshToken', {
//             refreshToken: getState().auth.refreshTokenJWT
//         }).then(res => {
//             // const dispatch = useDispatch()
//             console.log("res: ", res)
//             console.log("res.data.data.token: ", res.data.data.token)
//             // store.getState().auth.tokenJWT = res.data.data.token
//             dispatch({
//                 type: REFRESH_TOKEN,
//                 payload: newToken
//             })
//             setAuthToken(res.data.data.token)
//             return axios(originalReq)
//         }).catch(err => {
//             console.log("ERRO é: ", err)
//         })
//     };
// };

export const clearErrMsg = () => ({
    type: CLEAR_ERR_MSG
})

export const getBackendMode = () => {
    return (dispatch) => {
        axios.get('/')
            .then((res) => {
                console.log("getBackendMode-res: ", res.data)
                dispatch({
                    type: GET_BACKEND_MODE,
                    payload: res.data.data.singeUser
                })

                if (res.data.data.singeUser) {
                    dispatch({
                        type: LOGIN_SINGLE_USER,
                    })
                }
            })
            .catch((err) => {
                console.log(err)
            });
    };
}