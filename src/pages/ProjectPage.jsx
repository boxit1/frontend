import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Box, Button, Container, CssBaseline, Grid, Typography, withStyles} from "@material-ui/core";
import PublishIcon from "@material-ui/icons/Publish";
import {
    deleteProject,
    dowloadProject,
    startInstance,
    startProject,
    stopInstance,
    stopProject
} from "../store/actions/projects";
import DeleteIcon from "@material-ui/icons/Delete";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import Modal from "../components/Modal";
import InstancesCollapsibleTable from "../components/InstancesCollapsibleTable";
import {BounceLoader} from "react-spinners";

const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(5),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }, deleteBtn: {
        backgroundColor: "rgba(255,34,34,0.82)"
    },
});

class ProjectPage extends Component {
    state = {
        isModalOpen: false,
        isStartingProject: false,
        isStopingProject: false,
    }

    render() {
        const {classes} = this.props;
        let uuid = this.props.location.state.project.projectUUID
        let project = this.props.stateProjects.find(element => {
            return element.projectUUID === uuid
        })

        let allRunning = false
        if (project.instances && project.instances.length) {
            allRunning = project.instances.every(i => {
                return i.state === "running"
            })
        }

        return (

            <Container maxWidth="lg">
                <Box py={3}>
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Grid container component="main" spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="h4">
                                    {project.projectName}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <b> Uuid: </b> {project.projectUUID}
                            </Grid>
                            <Grid item xs={12}>
                                <b> state: </b> {project.state}
                            </Grid>
                            <Grid item xs={12}>
                                <b> Created At: </b> {project.createdAt}
                            </Grid>
                            <Grid item xs={12}>
                                <b>Last Updated at:</b> {project.updatedAt}
                            </Grid>
                            {project.instances !== undefined && project.instances.length ?
                                <Grid item xs={12}>
                                    <InstancesCollapsibleTable
                                        components={project.instances}
                                        startFunction={this.props.startInstance}
                                        stopFunction={this.props.stopInstance}
                                    />
                                </Grid>
                                :
                                <Grid item xs={12}>
                                    <Typography variant="h5">
                                        Project is not running
                                    </Typography>
                                </Grid>
                            }
                            <Grid item xs={12}>
                                <Button
                                    startIcon={<PublishIcon style={{transform: "rotate(180deg)"}}/>}
                                    type="button"
                                    variant="contained"
                                    color="secondary"
                                    onClick={() => this.props.downloadProject(project.projectUUID, project.projectName)}
                                >
                                    Download Project
                                </Button>
                            </Grid>
                            <Grid item xs={6} md={3}>
                                <Button
                                    startIcon={<DeleteIcon/>}
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    className={classes.deleteBtn}
                                    onClick={() =>
                                        this.setState({isModalOpen: true})
                                    }
                                >
                                    Delete Project
                                </Button>
                            </Grid>
                            <Grid item xs={6} md={3}>
                                <Button
                                    startIcon={(!this.state.isStopingProject ? <StopIcon/> : <BounceLoader size={20}/>)}
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    // className={classes.submit}
                                    disabled={
                                        this.state.isStopingProject
                                        || this.state.isStartingProject
                                        || project.instances === undefined
                                    }
                                    onClick={() => {
                                        this.setState({
                                            ...this.state,
                                            isStopingProject: true
                                        })
                                        this.props.stopProject(project.projectUUID)
                                    }}
                                >
                                    Stop Project
                                </Button>
                            </Grid>
                            <Grid item xs={6} md={3}>
                                <Button
                                    startIcon={(!this.state.isStartingProject ? <PlayArrowIcon/> :
                                        <BounceLoader size={20}/>)}
                                    type="button"
                                    variant="contained"
                                    color="secondary"
                                    disabled={
                                        this.state.isStartingProject
                                        || this.state.isStopingProject
                                        || allRunning}
                                    // className={classes.submit}
                                    onClick={() => {
                                        this.setState({
                                            ...this.state,
                                            isStartingProject: true
                                        })
                                        this.props.startProject(project.projectUUID)
                                    }}
                                >
                                    Start Project
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                    <Modal open={this.state.isModalOpen}
                           onClose={() => this.setState({...this.state, isModalOpen: false})}
                           onDeleteClick={() => this.props.deleteProject(project.projectUUID)}>
                        Are you sure you want to delete this project?
                    </Modal>
                </Box>
            </Container>
        );
    }

}

const mapDispatchToProps = (dispatch) => ({
    downloadProject: (uuid, projectName) => dispatch(dowloadProject(uuid, projectName)),
    deleteProject: (uuid) => dispatch(deleteProject(uuid)),
    startProject: (uuid) => dispatch(startProject(uuid)),
    stopProject: (uuid) => dispatch(stopProject(uuid)),
    startInstance: (id) => dispatch(startInstance(id)),
    stopInstance: (id) => dispatch(stopInstance(id)),
});

function mapStateToProps(state) {
    return {
        stateProjects: state.projects.projects
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)((ProjectPage)));