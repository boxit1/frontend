import axios from "axios";
import {ADD_PROJECT, DELETE_PROJECT, GET_PROJECTS, UPDATE_PROJECT} from "./actionTypes";

export const fetchProjects = () => {
    return (dispatch) => {
        axios.get('/projects/state')
            .then((res) => {
                // console.log("fetchProjects-axios.then-res.data.data.result: ", res.data.data.result)
                dispatch({
                    type: GET_PROJECTS,
                    payload: res.data.data.projects
                })
            })
            .catch((err) => {
                console.log(JSON.stringify(err))
                // dispatch({
                //     type: SIGNUP_SHOW_ERR_MSG,
                //     payload: err.response.data.message
                // })
            });
    };
};

export const newProject = (projectData) => {
    console.log(projectData)

    //work with projectData
    let formData = new FormData()

    /*
    --------------------!!!!!!!GOLDEN SHIT RIGHT HERE!!!!!!!--------------------

                    formData.append("project", projectData.file)
                    formData.append("instances[0][name]", "server")
                    formData.append("instances[0][fileName]", "./server.jar")
                    formData.append("instances[0][arguments][0]", "")
                    formData.append("instances[1][name]", "client")
                    formData.append("instances[1][fileName]", "./client.jar")
                    formData.append("instances[1][arguments][0]", "")
                    formData.append("instances[1][dependsOn][0]", "server")
    */

    var file = projectData.file
    console.log("newProject: ", file)   //TODO: a linha a baixo não funciona

    // Isto é para trocar o titulo do projeto
    // O titulo do projeto no backed é dado pelo nome do ficheiro, mudando aqui o nome do ficheio
    // muda-se o titulo do projeto :)
    var blob = file.slice(0, file.size, 'application/x-zip-compressed');
    var newFile = new File([blob], `${projectData.projectTitle}.zip`, {type: 'application/x-zip-compressed'});

    console.log("newProject(2): ", newFile)
    formData.append("project", newFile)
    let count = 0
    const helper: Array = [];
    projectData.inputFields.forEach((instance, i) => {
        for (let k = 0; k < instance.numInstances; k++) {
            if (instance.numInstances === 1) {
                formData.append(`instances[${count}][name]`, `${instance.instanceName}`)
                helper.push(`${instance.instanceName}`)
            }else{
                formData.append(`instances[${count}][name]`, `${instance.instanceName}${k}`)
                helper.push(`${instance.instanceName}${k}`)
            }
            formData.append(`instances[${count}][fileName]`, `./${instance.fileName}`)

            if (instance.args) {
                let temp
                temp = instance.args.replace(" ", "")
                temp = temp.split(",")
                console.log("temp: ", temp)
                temp.forEach((arg, index) => {
                    formData.append(`instances[${count}][arguments][${index}]`, arg)
                })
                console.log("instance.args: ", instance.args)
            } else {
                formData.append(`instances[${count}][arguments][0]`, "")
            }

            instance.dependsOn.forEach((dependency, j) => {
                    helper.every((iName) => {
                        if (iName.includes(dependency)) {
                            formData.append(`instances[${count}][dependsOn][${j}]`, iName)
                            return false
                        }
                        return true
                    })
                }
            )
            count++
        }
        console.log("fromData: ", Array.from(formData))

    })

    return (dispatch) => {
        axios.post('/projects/add', formData)
            .then((res) => {
                dispatch({
                    type: ADD_PROJECT,
                    payload: res.data.data.result
                })
                window.location.href = "/projects"
            })
            .catch((err) => {
                console.log(JSON.stringify(err))
                // dispatch({
                //     type: SIGNUP_SHOW_ERR_MSG,
                //     payload: err.response.data.message
                // })
            });
    };
};

export const deleteProject = (uid) => {
    console.log("deleteProject-UUID: ", uid)
    return (dispatch) => {
        console.log("WELELEL")
        // var config = {
        //     headers: {
        //         "authorization": axios.defaults.headers.common['Authorization'],
        //         "user-agent": " Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36",
        //         "accept": "*/*",
        //         "host": "localhost:5000",
        //         "accept-encoding": "gzip, deflate, br",
        //         "connection": "keep-alive",  }
        // };
        axios.delete(`/projects/${uid}`/*,config*/)
            .then((res) => {
                console.log("WELELELEL222")
                console.log("deleteProject-axios.then-res.data: ", res.data)
                console.log("deleteProject-axios.then-res.data.data: ", res.data.data)
                console.log("deleteProject-axios.then-res.data.data.result: ", res.data.data.result)
                dispatch({
                    type: DELETE_PROJECT,
                    payload: res.data.data.result
                })
            })
            .catch((err) => {
                console.log(JSON.stringify(err))
                // dispatch({
                //     type: SIGNUP_SHOW_ERR_MSG,
                //     payload: err.response.data.message
                // })
            });
    };
};

export const dowloadProject = (uuid, projectName) => {
    return () => {
        axios.get(`/projects/send/${uuid}`, {responseType: 'blob'})
            .then((res) => {
                const url = window.URL.createObjectURL(new Blob([res.data]))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', `${projectName}.zip`,)
                document.body.appendChild(link)
                link.click()
            })
            .catch((err) => {
                console.error(err)
            });
    }
}
export const fetchProject = (uuid, update, dispatch) => {
    axios.get(`/projects/state/${uuid}`)
        .then((res) => {
            console.log("fetchProject", res)
            if (update) {
                dispatch({
                    type: UPDATE_PROJECT,
                    payload: {
                        projectUuid: uuid,
                        project: res.data.data.projects[0]
                    }
                })
                window.location.reload();
            } else {
                //caso seja necessario
            }
        })
        .catch((err) => {
            console.log(JSON.stringify(err))
        });
};

export const startProject = (uuid) => {
    console.log(uuid)
    return (dispatch) => {
        axios.post(`/projects/up/${uuid}`)
            .then((res) => {
                console.log("startProject-res: ", res)
                fetchProject(uuid, true, dispatch)
            })
            .catch((err) => {
                console.error(err)
            });
    }
}
export const stopProject = (uuid) => {
    return (dispatch) => {
        axios.post(`/projects/down/${uuid}`)
            .then((res) => {
                console.log("stopProject-res: ", res)
                dispatch({
                    type: UPDATE_PROJECT,
                    payload: {
                        projectUuid: uuid,
                        project: {
                            ...res.data.data.result,
                            projectUUID: res.data.data.result.uuid,
                            projectName: res.data.data.result.name
                        }
                    }
                })
                window.location.reload();
            })
            .catch((err) => {
                console.error(err)
            });
    }
}
export const startInstance = (id) => {
    console.log(id)
    return (dispatch) => {
        axios.post(`/projects/instance/up/${id}`)
            .then((res) => {
                fetchProject(res.data.data.fromProject, true, dispatch)
            })
            .catch((err) => {
                console.error(err)
            });
    }
}

export const stopInstance = (id) => {
    return (dispatch) => {
        axios.post(`/projects/instance/down/${id}`)
            .then((res) => {
                fetchProject(res.data.data.fromProject, true, dispatch)
            })
            .catch((err) => {
                console.error(err)
            });
    }
}
