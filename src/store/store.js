import thunk from 'redux-thunk';
import {applyMiddleware, compose, createStore} from 'redux';
import RootReducer from "./reducers";
import {loadState} from "../utils/localStorage";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const initialState = loadState();

export default () => {
    const store = createStore(
        RootReducer,
        initialState,
        composeEnhancers(applyMiddleware(thunk))
    );

    return store;
};
