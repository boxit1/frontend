import React, {useMemo} from 'react';
import {useDropzone} from 'react-dropzone';
import {makeStyles} from "@material-ui/core";

const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '10px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
};

const activeStyle = {
    borderColor: '#2196f3'
};

const acceptStyle = {
    borderColor: '#00e676'
};

const rejectStyle = {
    borderColor: '#ff1744'
};

const useStyles = makeStyles((theme) => ({
    removeMargin: {
        margin: 7
    }
}));

export default function DropZone(props) {
    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,
        acceptedFiles
    } = useDropzone({
        accept: 'application/x-zip-compressed, application/zip',
        maxFiles: 1,
        disabled: props.disabled,
        onDropAccepted: acceptedFiles => {
            console.log(props)
            console.log(acceptedFiles[0])
            props.parentState.file = acceptedFiles[0]
            props.fileAdded(true)
        },
    });

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    const classes = useStyles();

    const files = acceptedFiles.map(file => (
        <>
            {file.path} - {file.size} bytes
        </>
    ));

    return (
        <div className="container">
            <div {...getRootProps({style})}>
                <input {...getInputProps()} />
                <p className={classes.removeMargin}>Drag 'n' drop the Project zip, or click to select it</p>
                <h4 className={classes.removeMargin}>File: <b>{files}</b></h4>
            </div>
        </div>
    );
}
