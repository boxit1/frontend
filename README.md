# Boxit Frontend

## Requirements

- [NodeJs](https://nodejs.org)
- [NPM](https://www.npmjs.com/) (usually installed with NodeJs)
- [Docker](https://www.docker.com/) (if you what to run the frontend inside a container)

## Instructions

### Change the API URL on the /src/index.js file if nedded
```js
axios.defaults.baseURL = "http://localhost:5000/api/";
```

## 1. Development
### 1.1 Install dependencies 
```console
npm install
```

### 1.2 Start the project
```console
npm start
```

## 2. Production/Deploy
### 2.1. Using Docker containers
### 2.1.1 run with docker-compose
```console
docker-compose up
```

### 2.2 Native
### 2.2.1 Install dependencies
```console
npm install
```
### 2.2.2 Install serve
```console
npm install -g serve
```
### 2.2.3 Build the project
```console
npm run build
```
### 2.2.2 Serve the application
```console
serve -s build
```


If using production, you have to build the app each time before serving.
