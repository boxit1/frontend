import React from 'react';
import {Link} from 'react-router-dom';

const NotFoundPage = (props) => (
    <div
        style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "90vh",

        }}>
        <h1>404 - <Link to="/" style={{textDecoration: 'none',}}><span>Go home</span></Link></h1>
    </div>
);

export default NotFoundPage;
