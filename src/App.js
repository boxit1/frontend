import './App.css';
import AppRouter from "./routers/appRouter";
import {MuiThemeProvider, Paper} from "@material-ui/core";
import {theme} from "./theme/theme";
import {Provider} from "react-redux";
import Store from "./store/store";
import React from "react";
import {saveState} from "./utils/localStorage";
import axios from "axios";
import setAuthToken from "./utils/setAuthToken";
import {getBackendMode, logout, refreshToken} from "./store/actions/auth";


const store = Store();
store.subscribe(() => {
    console.log(store.getState());
    saveState(store.getState())
});

function defineInterceptor() {
    axios.interceptors.response.use(response => {
            return response
        }, err => {
            return new Promise(((resolve, reject) => {
                const originalReq = err.config
                if (err.response.status === 401 && err.config && !err.config._retry) {
                    originalReq._retry = true
                    let res = axios.post("/refreshToken", {
                        refreshToken: store.getState().auth.refreshTokenJWT
                    }).then(res => {
                        store.dispatch(refreshToken(res.data.data.token))
                        setAuthToken(res.data.data.token)
                        originalReq.headers.Authorization = `Bearer ${res.data.data.token}`
                        return axios(originalReq)
                    }).catch(err => {
                        console.log("ERRO é (interceptor): ", err)
                    })
                    resolve(res)
                } else if (err.response.status === 400 && err.config && window.location.pathname !== '/login' && window.location.pathname !== '/') {
                    store.dispatch(logout())
                    window.location.href = '/login'
                } else {
                    reject(err)
                }
            }))
        }
    )
}

const App = () => {
    store.dispatch(getBackendMode())

    if (!store.getState().auth.backendSingleUser) {
        defineInterceptor()

        setAuthToken(store.getState().auth.tokenJWT)
        axios.post("/refreshToken", {
            refreshToken: store.getState().auth.refreshTokenJWT
        }).then(res => {
            store.dispatch(refreshToken(res.data.data.token))
            setAuthToken(res.data.data.token)
        }).catch(err => {
            console.log("ERRO é: ", JSON.stringify(err))
            // store.dispatch(logout())
        })
    }


    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <Paper style={{height: '100%'}}>
                    <AppRouter/>
                </Paper>
            </MuiThemeProvider>
        </Provider>
    )
};


export default App;