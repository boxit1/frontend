import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Box, Button, Container, CssBaseline, Grid, withStyles} from "@material-ui/core";
import ProjectCard from "../components/ProjectCard";
import {deleteProject, fetchProjects} from "../store/actions/projects";


const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(5),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }, addProjectBtn: {
        display: 'inline-block',
        textAlign: 'left',
        margin: theme.spacing(2),
        marginBottom: theme.spacing(4)
    }
});

function mapStateToProps(state) {
    return {
        projects: state.projects.projects
    };
}

function mapDispatchToProps(dispatch) {
    return {
        deleteProject: (uuid) => dispatch(deleteProject(uuid)),
        getProjects: () => dispatch(fetchProjects())
    };
}


class ProjectsPage extends Component {

    onNewProjectBtnClick = () => {
        this.props.history.push("/newProject")
    }

    componentDidMount() {
        this.props.getProjects();
        // console.log("this.props.projects: ", this.props.projects)
    };


    render() {
        const {classes} = this.props;

        return (
            <Container maxWidth="md">
                <Box py={3}>
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Grid container>
                            <Grid item>
                                <Button
                                    type="button"
                                    variant="contained"
                                    color="primary"
                                    className={classes.addProjectBtn}
                                    onClick={() => this.onNewProjectBtnClick()}
                                >
                                    New Project
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container component="main" spacing={2}>
                            {
                                this.props.projects.map((p) => {
                                        // console.log(p)
                                        return <Grid item xs={6} sm={4}>
                                            <ProjectCard title={p.projectName}
                                                         state={p.state}
                                                         runningInst={p.instances ? Array.from(p.instances).filter(value => value.state === "running").length : 0}
                                                         onDelete={() => this.props.deleteProject(p.projectUUID)}
                                                         propsToLink={{
                                                             pathname: "/project",
                                                             state: {
                                                                 project: p
                                                             }
                                                         }}
                                            />
                                            {/*onClick={this.props.deleteProject(p.uuid)}*/}
                                        </Grid>
                                    }
                                )
                            }
                            {/*<Grid item xs={6} sm={4}>*/}
                            {/*    <ProjectCard title={"WELELE"} description={"Daqui para a frente é tudo hardcoded"}/>*/}
                            {/*</Grid>*/}
                            {/*<Grid item xs={6} sm={4}>*/}
                            {/*    <ProjectCard/>*/}
                            {/*</Grid>*/}
                            {/*<Grid item xs={6} sm={4}>*/}
                            {/*    <ProjectCard/>*/}
                            {/*</Grid>*/}
                            {/*<Grid item xs={6} sm={4}>*/}
                            {/*    <ProjectCard/>*/}
                            {/*</Grid>*/}
                            {/*<Grid item xs={6} sm={4}>*/}
                            {/*    <ProjectCard/>*/}
                            {/*</Grid>*/}
                        </Grid>
                    </div>
                </Box>
            </Container>
            //
            // <div
            //     style={{
            //         display: "flex",
            //         justifyContent: "center",
            //         alignItems: "center",
            //         height: "90vh",
            //     }}>
            //     <h1>
            //         Projects Page
            //     </h1>
            // </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(ProjectsPage)));
