FROM node:alpine
WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .
RUN npm install
RUN npm install -g serve

COPY . .

RUN npm run build

EXPOSE ${5000}
