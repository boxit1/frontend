import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import LoginPage from '../pages/LoginPage';
import HomePage from "../pages/HomePage";
import NotFoundPage from '../pages/NotFoundPage';
import LandingPage from "../pages/LandingPage";
import NavBar from "../components/NavBar";
import SignUp from "../pages/SignUp";
import ProjectsPage from "../pages/ProjectsPage";
import SignUpSuccess from "../pages/SignUpSuccess";
import NewProjectPage from "../pages/NewProjectPage";
import ProjectPage from "../pages/ProjectPage";
import PrivateRoute from "./privateRouter";
import InstancesCollapsibleTable from "../components/InstancesCollapsibleTable";


const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Route path='/'
                   render={(history) => (
                       // <NavBar2 history={history}/>
                       <NavBar history={history}/>
                   )}>
            </Route>
            <Switch>
                <Route path="/" component={LandingPage} exact={true}/>
                <Route path="/login" component={LoginPage}/>
                <Route path="/signup" component={SignUp}/>
                <Route path="/signUpSuccess" component={SignUpSuccess}/>
                <Route path="/home" component={HomePage}/>
                <PrivateRoute path="/projects" component={ProjectsPage}/>
                <PrivateRoute path="/newProject" component={NewProjectPage}/>
                <PrivateRoute path="/project" component={ProjectPage}/>
                <PrivateRoute path="/table" component={InstancesCollapsibleTable}/>
                {/*<PublicRoute path="/home"" component={LoginPage}/>*/}
                {/*<PublicRoute path="/signup" component={SignUp}/>*/}
                {/*<PublicRoute path="/" component={LandingPage} exact={true}/>*/}
                {/*<PublicRoute path="/home" component={HomePage}/>*/}

                {/*<PrivateRoute path="/home" component={HomePage}/>*/}

                <Route component={NotFoundPage}/>


                {/*<PublicRoute path="/about" component={AboutPage} />*/}
                {/*<PublicRoute path="/contact" component={ContactPage} />*/}
                {/*<PublicRoute path="/FAQ" component={FAQPage} />*/}

                {/*<Route path="/services" component={ServicesPage} />*/}
            </Switch>

        </div>
    </BrowserRouter>
);

export default AppRouter;
