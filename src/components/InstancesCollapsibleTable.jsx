import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {Button, Grid} from "@material-ui/core";
import StopIcon from "@material-ui/icons/Stop";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";

const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

function Row(props) {
    const {instance, stopFunction, startFunction} = props;
    const [open, setOpen] = React.useState(false);
    const [isStopping, setIsStopping] = React.useState(false);
    const [isStarting, setIsStarting] = React.useState(false);
    const classes = useRowStyles();
    console.log("table instance: ", instance)
    console.log("table props: ", props)

    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {instance.name}
                </TableCell>
                <TableCell align="right">{instance.state}</TableCell>
                <TableCell align="right">{instance.ip}</TableCell>
                <TableCell align="right">{instance.id}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                Operations
                            </Typography>
                            <Grid container component="main" spacing={2}>
                                <Grid item xs={3}><Button
                                    startIcon={<PlayArrowIcon/>}
                                    type="button"
                                    variant="contained"
                                    color="secondary"
                                    onClick={() => {
                                        setIsStarting(true)
                                        return startFunction(instance.id);
                                    }}
                                    disabled={instance.state === 'running' || isStarting}
                                >
                                    Start Instance
                                </Button>
                                </Grid>
                                <Grid item xs={3}>

                                    <Button
                                        startIcon={<StopIcon/>}
                                        type="button"
                                        variant="contained"
                                        color="primary"
                                        onClick={() => {
                                            setIsStopping(true)
                                            return stopFunction(instance.id)
                                        }}
                                        disabled={instance.state === 'exited' || isStopping}
                                    >
                                        Stop Instance
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

Row.propTypes = {
    row: PropTypes.shape({
        calories: PropTypes.number.isRequired,
        carbs: PropTypes.number.isRequired,
        fat: PropTypes.number.isRequired,
        history: PropTypes.arrayOf(
            PropTypes.shape({
                amount: PropTypes.number.isRequired,
                customerId: PropTypes.string.isRequired,
                date: PropTypes.string.isRequired,
            }),
        ).isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        protein: PropTypes.number.isRequired,
    }).isRequired,
};


export default function InstancesCollapsibleTable({components, startFunction, stopFunction}) {
    console.log("components: ", components)
    console.log("typeof components: ", typeof components)
    if (components !== null) {
        console.log("components.length: ", components.length)
    }

    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell/>
                        <TableCell>Name</TableCell>
                        <TableCell align="right">State</TableCell>
                        <TableCell align="right">IP</TableCell>
                        <TableCell align="right">Id</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {components.map((instance) => (
                        <Row key={instance.name} instance={instance} startFunction={startFunction}
                             stopFunction={stopFunction}/>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}