import {CLEAR_ERR_MSG, SIGNUP_SHOW_ERR_MSG, SIGNUP_SHOW_MSG} from "../actions/actionTypes";

const defaultState = {}

export default (state = defaultState, action) => {

    switch (action.type) {
        case SIGNUP_SHOW_MSG: {
            console.log("signupReducer-action: ", action)
            return {
                signUpMsg: action.payload,
                msgType: 'success'
            };
        }
        case SIGNUP_SHOW_ERR_MSG: {
            return {
                signUpMsg: action.payload,
                msgType: 'error'
            };
        }
        case CLEAR_ERR_MSG:
            // console.log("CLEAR_ERR_MSG RUNNED")
            return {
                ...state,
                signUpMsg: ""
            };
        default:
            return state;
    }
};
