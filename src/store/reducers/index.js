import {combineReducers} from "redux";
import authReducer from './auth'
import signupReducer from './signup'
import projectsReducer from "./projects";
import {USER_LOGOUT} from "../actions/actionTypes";

const AppReducer = combineReducers({
    signUp: signupReducer,
    auth: authReducer,
    projects: projectsReducer,
});

const RootReducer = (state, action) => {
    if (action.type === USER_LOGOUT) {
        return AppReducer(undefined, action)
    }
    return AppReducer(state, action)
}

export default RootReducer;
